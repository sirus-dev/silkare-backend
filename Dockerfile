FROM node:11.1.0-alpine
COPY . /app
WORKDIR /app 
RUN npm install --production
EXPOSE 1337
ENTRYPOINT [ "npm", "run"]
CMD [ "start"]