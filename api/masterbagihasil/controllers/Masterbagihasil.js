'use strict';

/**
 * Masterbagihasil.js controller
 *
 * @description: A set of functions called "actions" for managing `Masterbagihasil`.
 */

module.exports = {

  /**
   * Retrieve masterbagihasil records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.masterbagihasil.search(ctx.query);
    } else {
      return strapi.services.masterbagihasil.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a masterbagihasil record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.masterbagihasil.fetch(ctx.params);
  },

  /**
   * Count masterbagihasil records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.masterbagihasil.count(ctx.query);
  },

  /**
   * Create a/an masterbagihasil record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.masterbagihasil.add(ctx.request.body);
  },

  /**
   * Update a/an masterbagihasil record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.masterbagihasil.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an masterbagihasil record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.masterbagihasil.remove(ctx.params);
  },

  /**
   * Add relation to a/an masterbagihasil record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.masterbagihasil.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an masterbagihasil record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.masterbagihasil.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an masterbagihasil record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.masterbagihasil.removeRelation(ctx.params, ctx.request.body);
  }
};
