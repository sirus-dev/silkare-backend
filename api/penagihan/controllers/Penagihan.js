'use strict';

/**
 * Penagihan.js controller
 *
 * @description: A set of functions called "actions" for managing `Penagihan`.
 */

module.exports = {

  /**
   * Retrieve penagihan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.penagihan.search(ctx.query);
    } else {
      return strapi.services.penagihan.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a penagihan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.penagihan.fetch(ctx.params);
  },

  /**
   * Count penagihan records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.penagihan.count(ctx.query);
  },

  /**
   * Create a/an penagihan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.penagihan.add(ctx.request.body);
  },

  /**
   * Update a/an penagihan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.penagihan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an penagihan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.penagihan.remove(ctx.params);
  },

  /**
   * Add relation to a/an penagihan record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.penagihan.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an penagihan record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.penagihan.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an penagihan record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.penagihan.removeRelation(ctx.params, ctx.request.body);
  }
};
