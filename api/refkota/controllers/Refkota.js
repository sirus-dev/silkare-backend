'use strict';

/**
 * Refkota.js controller
 *
 * @description: A set of functions called "actions" for managing `Refkota`.
 */

module.exports = {

  /**
   * Retrieve refkota records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.refkota.search(ctx.query);
    } else {
      return strapi.services.refkota.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a refkota record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.refkota.fetch(ctx.params);
  },

  /**
   * Count refkota records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.refkota.count(ctx.query);
  },

  /**
   * Create a/an refkota record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.refkota.add(ctx.request.body);
  },

  /**
   * Update a/an refkota record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.refkota.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an refkota record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.refkota.remove(ctx.params);
  },

  /**
   * Add relation to a/an refkota record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.refkota.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an refkota record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.refkota.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an refkota record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.refkota.removeRelation(ctx.params, ctx.request.body);
  }
};
