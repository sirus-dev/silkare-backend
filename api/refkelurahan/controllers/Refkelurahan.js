'use strict';

/**
 * Refkelurahan.js controller
 *
 * @description: A set of functions called "actions" for managing `Refkelurahan`.
 */

module.exports = {

  /**
   * Retrieve refkelurahan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.refkelurahan.search(ctx.query);
    } else {
      return strapi.services.refkelurahan.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a refkelurahan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.refkelurahan.fetch(ctx.params);
  },

  /**
   * Count refkelurahan records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.refkelurahan.count(ctx.query);
  },

  /**
   * Create a/an refkelurahan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.refkelurahan.add(ctx.request.body);
  },

  /**
   * Update a/an refkelurahan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.refkelurahan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an refkelurahan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.refkelurahan.remove(ctx.params);
  },

  /**
   * Add relation to a/an refkelurahan record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.refkelurahan.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an refkelurahan record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.refkelurahan.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an refkelurahan record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.refkelurahan.removeRelation(ctx.params, ctx.request.body);
  }
};
