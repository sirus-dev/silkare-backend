'use strict';

/**
 * Penagihandetail.js controller
 *
 * @description: A set of functions called "actions" for managing `Penagihandetail`.
 */

module.exports = {

  /**
   * Retrieve penagihandetail records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.penagihandetail.search(ctx.query);
    } else {
      return strapi.services.penagihandetail.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a penagihandetail record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.penagihandetail.fetch(ctx.params);
  },

  /**
   * Count penagihandetail records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.penagihandetail.count(ctx.query);
  },

  /**
   * Create a/an penagihandetail record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.penagihandetail.add(ctx.request.body);
  },

  /**
   * Update a/an penagihandetail record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.penagihandetail.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an penagihandetail record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.penagihandetail.remove(ctx.params);
  },

  /**
   * Add relation to a/an penagihandetail record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.penagihandetail.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an penagihandetail record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.penagihandetail.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an penagihandetail record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.penagihandetail.removeRelation(ctx.params, ctx.request.body);
  }
};
