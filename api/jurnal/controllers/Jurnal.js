'use strict';

/**
 * Jurnal.js controller
 *
 * @description: A set of functions called "actions" for managing `Jurnal`.
 */

module.exports = {

  /**
   * Retrieve jurnal records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.jurnal.search(ctx.query);
    } else {
      return strapi.services.jurnal.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a jurnal record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.jurnal.fetch(ctx.params);
  },

  /**
   * Count jurnal records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.jurnal.count(ctx.query);
  },

  /**
   * Create a/an jurnal record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.jurnal.add(ctx.request.body);
  },

  /**
   * Update a/an jurnal record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.jurnal.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an jurnal record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.jurnal.remove(ctx.params);
  },

  /**
   * Add relation to a/an jurnal record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.jurnal.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an jurnal record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.jurnal.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an jurnal record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.jurnal.removeRelation(ctx.params, ctx.request.body);
  }
};
