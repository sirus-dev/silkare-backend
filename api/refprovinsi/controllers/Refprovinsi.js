'use strict';

/**
 * Refprovinsi.js controller
 *
 * @description: A set of functions called "actions" for managing `Refprovinsi`.
 */

module.exports = {

  /**
   * Retrieve refprovinsi records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.refprovinsi.search(ctx.query);
    } else {
      return strapi.services.refprovinsi.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a refprovinsi record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.refprovinsi.fetch(ctx.params);
  },

  /**
   * Count refprovinsi records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.refprovinsi.count(ctx.query);
  },

  /**
   * Create a/an refprovinsi record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.refprovinsi.add(ctx.request.body);
  },

  /**
   * Update a/an refprovinsi record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.refprovinsi.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an refprovinsi record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.refprovinsi.remove(ctx.params);
  },

  /**
   * Add relation to a/an refprovinsi record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.refprovinsi.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an refprovinsi record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.refprovinsi.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an refprovinsi record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.refprovinsi.removeRelation(ctx.params, ctx.request.body);
  }
};
