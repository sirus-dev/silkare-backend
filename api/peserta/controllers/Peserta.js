'use strict';

/**
 * Peserta.js controller
 *
 * @description: A set of functions called "actions" for managing `Peserta`.
 */

module.exports = {

  /**
   * Retrieve peserta records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.peserta.search(ctx.query);
    } else {
      return strapi.services.peserta.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a peserta record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.peserta.fetch(ctx.params);
  },

  /**
   * Count peserta records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.peserta.count(ctx.query);
  },

  /**
   * Create a/an peserta record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.peserta.add(ctx.request.body);
  },

  /**
   * Update a/an peserta record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.peserta.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an peserta record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.peserta.remove(ctx.params);
  },

  /**
   * Add relation to a/an peserta record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.peserta.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an peserta record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.peserta.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an peserta record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.peserta.removeRelation(ctx.params, ctx.request.body);
  }
};
