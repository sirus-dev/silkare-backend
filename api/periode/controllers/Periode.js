'use strict';

/**
 * Periode.js controller
 *
 * @description: A set of functions called "actions" for managing `Periode`.
 */

module.exports = {

  /**
   * Retrieve periode records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.periode.search(ctx.query);
    } else {
      return strapi.services.periode.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a periode record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.periode.fetch(ctx.params);
  },

  /**
   * Count periode records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.periode.count(ctx.query);
  },

  /**
   * Create a/an periode record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.periode.add(ctx.request.body);
  },

  /**
   * Update a/an periode record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.periode.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an periode record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.periode.remove(ctx.params);
  },

  /**
   * Add relation to a/an periode record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.periode.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an periode record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.periode.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an periode record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.periode.removeRelation(ctx.params, ctx.request.body);
  }
};
