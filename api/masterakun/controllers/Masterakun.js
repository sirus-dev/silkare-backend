'use strict';

/**
 * Masterakun.js controller
 *
 * @description: A set of functions called "actions" for managing `Masterakun`.
 */

module.exports = {

  /**
   * Retrieve masterakun records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.masterakun.search(ctx.query);
    } else {
      return strapi.services.masterakun.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a masterakun record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.masterakun.fetch(ctx.params);
  },

  /**
   * Count masterakun records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.masterakun.count(ctx.query);
  },

  /**
   * Create a/an masterakun record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.masterakun.add(ctx.request.body);
  },

  /**
   * Update a/an masterakun record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.masterakun.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an masterakun record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.masterakun.remove(ctx.params);
  },

  /**
   * Add relation to a/an masterakun record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.masterakun.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an masterakun record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.masterakun.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an masterakun record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.masterakun.removeRelation(ctx.params, ctx.request.body);
  }
};
