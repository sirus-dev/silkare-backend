'use strict';

/**
 * Perusahaan.js controller
 *
 * @description: A set of functions called "actions" for managing `Perusahaan`.
 */

module.exports = {

  /**
   * Retrieve perusahaan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.perusahaan.search(ctx.query);
    } else {
      return strapi.services.perusahaan.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a perusahaan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.perusahaan.fetch(ctx.params);
  },

  /**
   * Count perusahaan records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.perusahaan.count(ctx.query);
  },

  /**
   * Create a/an perusahaan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.perusahaan.add(ctx.request.body);
  },

  /**
   * Update a/an perusahaan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.perusahaan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an perusahaan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.perusahaan.remove(ctx.params);
  },

  /**
   * Add relation to a/an perusahaan record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.perusahaan.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an perusahaan record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.perusahaan.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an perusahaan record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.perusahaan.removeRelation(ctx.params, ctx.request.body);
  }
};
